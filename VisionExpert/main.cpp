/*
 * main.cpp
 *
 *  Created on: Mar 3, 2017
 *      Author: trucndt
 */

#include "SpokesmanPi2.h"

using namespace boost::program_options;
using namespace std;

variables_map gPROG_ARGUMENT;

int processCommandLineArgument(int argc, char **argv);

int main(int argc, char **argv)
{
    if (processCommandLineArgument(argc, argv) < 0)
    {
        return 0;
    }

    cout << "Built time: " << __DATE__ << " " << __TIME__ << endl;

    CameraController mCamControl;
    mCamControl.enableCamera();
    mCamControl.startCamera();

    vector<Json::Value> detectRes;
    LaneDetection mLaneDetection(&mCamControl);
    mLaneDetection.startDetect();
    cout << "Start.\n";

    SpokesmanPi2 mSpokesman(&mLaneDetection);
    mSpokesman.initialize();
    mSpokesman.listen();

    return 0;
}

int processCommandLineArgument(int argc, char **argv)
{
    options_description usage("Usage");

    usage.add_options()
            ("help,h", "Print help message");

    try
    {
        store(command_line_parser(argc, argv).options(usage).run(), gPROG_ARGUMENT);

        if (gPROG_ARGUMENT.count("help"))
        {
            cout << usage << endl;
            return -1;
        }

        notify(gPROG_ARGUMENT);
    }
    catch (required_option& e)
    {
        cout << "ERROR: " << e.what() << endl;
        cout << usage << endl;
        return -1;
    }
    catch (error& e)
    {
        cout << "ERROR: " << e.what() << endl;
        return -1;
    }

    return 0;
}
