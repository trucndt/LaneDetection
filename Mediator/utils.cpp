/*
 * utils.cpp
 *
 *  Created on: Aug 18, 2016
 *      Author: trucndt
 */

#include "utils.h"
#include <string.h>

int makeRealTimeThread()
{
	struct sched_param params;

	params.__sched_priority = 60; //should not get too high priority, may override system's modules
	return pthread_setschedparam(pthread_self(), SCHED_FIFO, &params);
}

void dieWithError(const char *anErrMess)
{
	std::cout << "ERR: " << anErrMess << std::endl;
	exit(1);
}

std::string getIPAddress(const char *aDev)
{
	int fd;
	struct ifreq ifr;

	fd = socket(AF_INET, SOCK_DGRAM, 0);

	/* I want to get an IPv4 IP address */
	ifr.ifr_addr.sa_family = AF_INET;

	/* I want an IP address attached to aDev */
	strncpy(ifr.ifr_name, aDev, IFNAMSIZ-1);

	ioctl(fd, SIOCGIFADDR, &ifr);

	close(fd);
    return std::string(inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));
}
