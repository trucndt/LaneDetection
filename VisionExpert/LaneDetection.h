/*
 * LaneDetection.h
 *
 *  Created on: May 10, 2017
 *      Author: baohq
 */

#ifndef VISIONEXPERT_LANEDETECTION_H_
#define VISIONEXPERT_LANEDETECTION_H_

#include <opencv2/opencv.hpp>
#include <boost/circular_buffer.hpp>
#include <string>
#include "CameraController.h"
#include "../Mediator/json/json.h"
#include <boost/date_time/posix_time/posix_time.hpp>

class LaneDetection {
public:
	LaneDetection();
	LaneDetection(CameraController *camctrl);
	virtual ~LaneDetection();
	cv::Mat detectLaneOfImage(cv::Mat inputImg);
	cv::Mat blurImage(cv::Mat img);
	cv::Mat filterImageColor(cv::Mat img, bool laneColor);
	cv::Mat cannyEdgeDetect(cv::Mat img);
	cv::Mat sobelEdgeDetect(cv::Mat img);
	cv::Mat binThreshold(cv::Mat colorTHImg, cv::Mat sobelImg);
	cv::Mat getROI(cv::Mat img);
	cv::Mat perspectiveTransform(cv::Mat img, bool perspective);
	cv::Mat thresholdImage(cv::Mat oriImg, bool laneColor=0);
	cv::Mat preProcessImage(cv::Mat, bool laneColor=1);
	Json::Value detectLines(cv::Mat);
	Json::Value lineFit(cv::Mat binImg);
	void detectLane(bool laneColor=0);
	Json::Value getLaneResult();

	void startDetect();

	static const bool DARK_LANE = 0;
	static const bool LIGHT_LANE = 1;

private:
	static const uint8_t kernelSize = 3;			//Kernel size for Gaussian filter
	static const uint8_t lowYHue = 0;//65;			//Min HUE's value for yellow
	static const uint8_t highYHue = 40;//105		//Max HUE's value for yellow
	static const uint8_t lowYSaturation = 80;//40;	//Min SATURATION's value for yellow
	static const uint8_t highYSaturation = 255;		//Max SATURATION's value for yellow
	static const uint8_t lowYValue = 200;//80;		//Min VALUE's value for yellow
	static const uint8_t highYValue = 255;			//Max VALUE's value for yellow

	static const uint8_t lowBHue = 25;				//Min HUE's value for black
	static const uint8_t highBHue = 120;			//Max HUE's value for black
	static const uint8_t lowBSaturation = 70;		//Min SATURATION's value for black
	static const uint8_t highBSaturation = 255;		//Max SATURATION's value for black
	static const uint8_t lowBValue = 50;			//Min VALUE's value for black
	static const uint8_t highBValue = 255;			//Max VALUE's value for black

	static const uint8_t lowWHue = 0;				//Min HUE's value for white
	static const uint8_t highWHue = 180;			//Max HUE's value for white
	static const uint8_t lowWSaturation = 0;		//Min SATURATION's value for white
	static const uint8_t highWSaturation = 100;		//Max SATURATION's value for white
	static const uint8_t lowWValue = 170;			//Min VALUE's value for white
	static const uint8_t highWValue = 255;			//Max VALUE's value for white

	static const int AHEAD_PIXELS = 420;			//Number of pixels ahead for detecting steering angle
	static const int ANGLE_OFFSET_PIXELS = 24;	 	//Satisfy 0.1rad minimum rotate angle of robot

	boost::circular_buffer<Json::Value> resultBuffer;
	Json::Value laneResult;

	CameraController *camController;
	cv::Mat defineROI(cv::Mat oriImg, const cv::Point **vertices, int numOfVertices);
	boost::mutex laneResultMtx;
};

#endif /* VISIONEXPERT_LANEDETECTION_H_ */
