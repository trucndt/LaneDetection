//
// Created by trucndt on 5/12/17.
//

#ifndef RCSEMBEDDED_LANELINECONTROL_H
#define RCSEMBEDDED_LANELINECONTROL_H

#include "Mediator.h"

class Mediator;
class ServoController;

class LanelineControl
{
public:
    LanelineControl(Mediator *aMediator,
                        ServoController *aServoController,
                        SpokesmanPi1 *aSpokesman);
    void start();
    static void getWheelRpm(float angle, int &leftRpm, int &rightRpm);

private:
    static const int QUERY_TIME_IN_MS = 300;
    static const double SPEED_M_S = 0.12;
    static const double SPEED_MAX = 0.62;

    int numOfFrames;

    Mediator* mMediator;
    ServoController* mServoController;
    SpokesmanPi1* mSpokesman;

    void run();
    void handlePositionUpdate(const char *aMsg, int aMsgSize);
};


#endif //RCSEMBEDDED_LANELINECONTROL_H
