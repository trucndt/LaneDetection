/*
 * LaneDetection.cpp
 *
 *  Created on: May 10, 2017
 *      Author: baohq
 */

#include "LaneDetection.h"

using namespace std;
using namespace cv;

LaneDetection::LaneDetection(CameraController* camctrl)
{
	camController = camctrl;
	resultBuffer.set_capacity(5);
}

LaneDetection::~LaneDetection()
{

}

/**
 * @brief blurs image
 * @param img the input image to be blurred
 * @return blurred image
 */
Mat LaneDetection::blurImage(Mat img)
{
	Mat blurImg;
	GaussianBlur(img, blurImg, Size(kernelSize, kernelSize), 0, 0, 0);

	return blurImg;
}

/**
 * @brief thresholds image according to specific color: dark colors or light colors
 * @param img the input image to be thresholded
 * @param laneColor specifies dark color (black) lane or light color (white) lane
 * @return thresholded binary image
 */
Mat LaneDetection::filterImageColor(Mat img, bool laneColor)
{
	Mat filteredImg, binImg, tmp = Mat::ones(img.rows, img.cols,img.type());
	Mat wImg, bImg;

	if (laneColor == LIGHT_LANE)
	{
		//Get image with only white pixels
		inRange(img, Scalar(lowWHue, lowWSaturation, lowWValue), Scalar(highWHue, highWSaturation, highWValue), wImg);

		//Convert to binary image
		threshold(wImg, binImg, 1, 255, THRESH_BINARY);

	}

	else if (laneColor == DARK_LANE)
	{
		//Get image with only black pixels
		inRange(img, Scalar(lowBHue, lowBSaturation, lowBValue), Scalar(highBHue, highBSaturation, highBValue), bImg);
		threshold(bImg, binImg, 40, 255, THRESH_BINARY);
	}

	return binImg;
}

Mat LaneDetection::cannyEdgeDetect(Mat img)
{
	Mat edgeImg;
	Canny(img, edgeImg, 30, 130);
	return edgeImg;
}

Mat LaneDetection::sobelEdgeDetect(Mat img)
{
	Mat gradX, absGradX, binImg;

	//X-direction sobel
	Sobel(img, gradX, CV_16S, 1, 0);
	//Get abs value of x gradient
	convertScaleAbs(gradX, absGradX);

	//Convert to binary image
	threshold(absGradX, binImg, 100, 255, THRESH_BINARY);

	//Use only the result of sobel x-direction
	return binImg;
}

Mat LaneDetection::binThreshold(cv::Mat colorTHImg, cv::Mat sobelImg)
{
	Mat thImg, tmp, g1, g2, mask;
	tmp = Mat::ones(sobelImg.rows, sobelImg.cols, sobelImg.type());

	//
	bitwise_or(colorTHImg, sobelImg, mask);
	//
	bitwise_and(sobelImg, colorTHImg, thImg, mask);
	//
	threshold(mask, thImg, 1, 255, THRESH_BINARY);

	return mask;
}

Mat LaneDetection::defineROI(cv::Mat oriImg, const Point **vertices, int numOfVertices)
{
	Mat mask, roiImg;

	mask = Mat::zeros(Size(oriImg.cols, oriImg.rows), oriImg.type());
	fillPoly(mask, vertices, &numOfVertices, 1, Scalar(255, 255, 255), 8);
	bitwise_and(oriImg, mask, roiImg);

	return roiImg;
}

//define a polygon region of interest - 6 edges
Mat LaneDetection::getROI(Mat img)
{
	Mat roiImg;//, mask;
	int imgWidth, imgHeight, numOfVertices = 6;
	imgWidth = img.cols;
	imgHeight = img.rows;

	//Define 6 points of polygon
	Point pointArray[1][6];
	pointArray[0][0] = Point(0, imgHeight);
	pointArray[0][1] = Point(0, imgHeight - 20);
	pointArray[0][2] = Point(imgWidth/3 + 40, 2*imgHeight/3+20);
	pointArray[0][3] = Point(2*imgWidth/3 - 40, 2*imgHeight/3+20);
	pointArray[0][4] = Point(imgWidth, imgHeight - 20);
	pointArray[0][5] = Point(imgWidth, imgHeight);

	const Point *vertices[1] = {pointArray[0]};
	roiImg = defineROI(img, vertices, numOfVertices);

	return roiImg;
}

Mat LaneDetection::perspectiveTransform(Mat img, bool perspective)
{
	Mat transImg = Mat(img.rows, img.cols, img.type()), warpMat;
	int imgWidth, imgHeight;
	imgWidth = img.cols;
	imgHeight = img.rows;

	//Define source points
	Point2f src1Points[] = {
			Point2f(0, imgHeight-30),
			Point2f(0+180, imgHeight/2+60),
			Point2f(imgWidth-80, imgHeight/2+60),
			Point2f(imgWidth, imgHeight-30)
	};

	//Define destination points
	Point2f src2Points[] = {
			Point2f(imgWidth/2-180, imgHeight),
			Point2f(imgWidth/2-180,0),
			Point2f(imgWidth/2+180, 0),
			Point2f(imgWidth/2+180, imgHeight)
	};

	//To top-down view
	if (perspective == 0)
	{
		warpMat = getPerspectiveTransform(src1Points, src2Points);
		warpPerspective(img, transImg, warpMat, Size(img.cols, img.rows), INTER_LINEAR, BORDER_CONSTANT, Scalar());
	}

	//To original view
	else
	{
		getPerspectiveTransform(src2Points, src1Points);
		warpPerspective(img, transImg, warpMat, Size(img.rows, img.cols), INTER_LINEAR, BORDER_CONSTANT, Scalar());
	}

	return transImg;
}

Json::Value LaneDetection::lineFit(Mat binImg)
{
	Json::Value root;
	Mat vizImg(Size(binImg.rows, binImg.cols), CV_8UC3);
	vector<Mat> aImg(3, binImg);
	merge(aImg, vizImg);

	vector<int> gLaneLineCenterX;
	vector<int> gWinCenterLeftX;
	vector<int> gWinCenterRightX;
	vector<int> histogramOnLeft(binImg.cols/2);
	vector<int> histogramOnRight(binImg.cols/2);

	Point baseLeftCoor, baseRightCoor, tmp1, tmp2;	//base* is coordinate of the pixels for starting lookup
	double unused1, unused2, leftPeak, rightPeak;	//leftPeak and rightPeak are highest value of nonzero pixels on a column
	const int noWindows = 9;
	const int margin = 50; //pixels
	const int minPixels = 50; //minimum acceptable pixels to re-center the window
	const int midPoint = binImg.cols/2;
	int windowHeight = binImg.rows/noWindows;
	int curLeftX, curRightX, totalLanePixels;

	Mat tmp = binImg.rowRange((binImg.rows)/2, binImg.rows);

	Mat leftPart = tmp.colRange(0, midPoint);
	Mat rightPart = tmp.colRange(midPoint, binImg.cols);

	//Mat histogram;
	reduce(leftPart, histogramOnLeft, 0, CV_REDUCE_SUM);
	reduce(rightPart, histogramOnRight, 0, CV_REDUCE_SUM);

	//Get starting point coordinate of lanelines
	minMaxLoc(histogramOnLeft, &unused1, &leftPeak, &tmp1, &baseLeftCoor);
	minMaxLoc(histogramOnRight, &unused2, &rightPeak, &tmp2, &baseRightCoor);

	curLeftX = baseLeftCoor.x;
	curRightX = baseRightCoor.x + midPoint;
	totalLanePixels = 0;

	for (int i = 0; i < noWindows; i++)
	{
		//Window's boundaries
		int winLowY = binImg.rows - (i+1)*windowHeight;
		int winHighY = binImg.rows - i*windowHeight;

		//Calculate width of window
		int winLeftLowX = curLeftX - margin;
		int winLeftHighX = curLeftX + margin;
		int winRightLowX = curRightX - margin;
		int winRightHighX = curRightX + margin;

		gWinCenterLeftX.push_back(curLeftX);	//Add current center point-x coordinate of left lane
		gWinCenterRightX.push_back(curRightX);	//Add current center point-x coordinate of right lane
		gLaneLineCenterX.push_back((curLeftX + curRightX)/2); //Add current center point-x coordinate of lanes

		//Identify nonzero pixels in window's range
		vector<int> winLeftNonZeroX;
		vector<int> winLeftNonZeroY;
		vector<int> winRightNonZeroX;
		vector<int> winRightNonZeroY;

		//To find mean coordinate to re-center window
		int sumLeftX = 0, sumRightX = 0;

		//Find nonzero pixel in range of a window
		for (int row = winLowY; row < winHighY; row++)
		{
			//Left part
			for (int col = winLeftLowX; col < winLeftHighX; col++)
			{
				if(binImg.at<char>(row, col) > 30)
				{
					sumLeftX += col;
					winLeftNonZeroX.push_back(col);
					winLeftNonZeroY.push_back(row);
					totalLanePixels++;
				}
			}

			//Right part
			for (int col = winRightLowX; col < winRightHighX; col++)
			{
				if(binImg.at<char>(row, col) > 30)
				{
					sumRightX += col;
					winRightNonZeroX.push_back(col);
					winRightNonZeroY.push_back(row);
				}
			}
		}
		//if nonzero pixel >= 50 pixel, re-center window -- left part
		if (winLeftNonZeroX.size() >= minPixels)
		{
			curLeftX = sumLeftX/winLeftNonZeroX.size();
		}

		//if nonzero pixel >= 50 pixel, re-center window -- right part
		if (winRightNonZeroX.size() >= minPixels)
		{
			curRightX = sumRightX/winRightNonZeroX.size();
		}
	}

	//Found no lane pixels
	cout << "total pixels: " << totalLanePixels << endl;
	if (totalLanePixels <= 600)
	{
		root["isLane"] = false;
		root["angle"] = 0.0;
	}

	else
	{
		//Detect vehicle offset from center of lane lines
		int startPoint = gLaneLineCenterX[0];	//belongs first window
		int aheadPoint = gLaneLineCenterX[AHEAD_PIXELS/windowHeight + 1]; //belongs fifth window
		int diff = startPoint - aheadPoint;

		float angle = atan(diff/(1.0*AHEAD_PIXELS));
		root["isLane"] = true;
		root["angle"] = angle;
	//	cout << "section4\n";

	}
	return root;
}

Mat LaneDetection::thresholdImage(cv::Mat oriImg, bool laneColor)
{

	//Process image
	//ROI
	Mat roiImg = getROI(oriImg);

	//To HSV color space
	Mat hsvImg;
	cvtColor(oriImg, hsvImg, CV_BGR2HSV);

	//Blur image
	Mat blurred = blurImage(hsvImg);

	//Get YW image
	Mat filteredImg = filterImageColor(blurred, 0);

	//Edge image
	Mat sobelImg = sobelEdgeDetect(filteredImg);

	//Threshold image
	Mat thImg;
	thImg = binThreshold(filteredImg, sobelImg);

	//top-down view
	Mat topdownImg = perspectiveTransform(thImg, 0);

	return topdownImg;
}

Mat LaneDetection::preProcessImage(Mat oriImg, bool laneColor)
{
	//ROI
	Mat roiImg = getROI(oriImg);

	//To HSV color space
	Mat hsvImg;
	cvtColor(roiImg, hsvImg, CV_BGR2HSV);

	//Blur image
	Mat blurred = blurImage(hsvImg);

	//Get YW image
	Mat filteredImg = filterImageColor(blurred, 1);

	//Edge image
	Mat sobelImg = sobelEdgeDetect(filteredImg);

	return sobelImg;
}

Json::Value LaneDetection::detectLines(Mat img)
{
	const double COFF = 1.50;
	const double ANGLE_OFFSET = 70.0;
	const double STRAIGHT_ANGLE_OFFSET = 40.0;
	Json::Value root;
	unsigned char direction = 0;	//1 is left curve lane, 2 is right curve lane;
	vector<Vec4i> lines;
	bool isLane = false;
	double steeringAngle = 0.0;
	Mat vizImg(Size(img.rows, img.cols), CV_8UC3);
	vector<Mat> aImg(3, img);
	vector<Vec4i> leftPoints, rightPoints;
	vector<Vec2d> leftLines, rightLines;

	int midPoint = img.cols/2;		//middle point of image
	merge(aImg, vizImg);
	HoughLinesP(img, lines, 1, CV_PI / 180.0, 70, 30, 20);

	//Calculate slope and y-intercept for each line - y = ax + b
	for (int i = 0; i < lines.size(); i++)
	{
		int x_diff = lines[i][2] - lines[i][0];
		int y_diff = lines[i][3] - lines[i][1];

		//slope - a
		double slope = (double)y_diff / (double)x_diff;

		//y-intercept - b
		double y_intercept = (double)lines[i][1] - slope * (double)lines[i][0];

		//Classify to left or right part - base on x-coordinate of start of lines
		if ((lines[i][0] < midPoint) && (lines[i][2] < midPoint))
		{
			leftPoints.push_back(lines[i]);
			leftLines.push_back(Vec2d(slope, y_intercept));
		}
		else if ((lines[i][0] > midPoint) && (lines[i][2] > midPoint))
		{
			rightPoints.push_back(lines[i]);
			rightLines.push_back(Vec2d(slope, y_intercept));
		}
		else
		{
			if (slope < 0)
			{
				leftPoints.push_back(lines[i]);
				leftLines.push_back(Vec2d(slope, y_intercept));
			}
			else
			{
				rightPoints.push_back(lines[i]);
				rightLines.push_back(Vec2d(slope, y_intercept));
			}
		}

	}

	//eliminate double lines in 1 lane (overlapping, parallel) base on angle between lines < 5.625 degree
	//Left part
	for (int i = 0; i < leftLines.size(); i++)
	{
		double thisAngle = atan(leftLines[i][0]);
		for (int j = 0; j < leftLines.size(); j++)
		{
			if (j == i) continue;
			double rAngle = fabs(atan(leftLines[j][0]));
			double diffAngle = fabs(180.0*(thisAngle - rAngle)/CV_PI);

			//two lines is nearly parallel
			if ((diffAngle < 15.0) || (diffAngle > 60.0 && (leftLines[i][0] * leftLines[j][0]) < 0))
			{
				if (fabs(leftLines[i][1]) > fabs(leftLines[j][1]))
				{
					leftLines.erase(leftLines.begin() + j);
					leftPoints.erase(leftPoints.begin() + j);
					j -= 1;
				}

				else
				{
					leftLines.erase(leftLines.begin() + i);
					leftPoints.erase(leftPoints.begin() + i);
					i -= 1;
					break;
				}
			}
		}
	}

	//eliminate double lines in 1 lane (overlapping, parallel) base on angle between lines < 5.625 degree
	//Right part
	for (int i = 0; i < rightLines.size(); i++)
	{
		double thisAngle = atan(rightLines[i][0]);
		for (int j = 0; j < rightLines.size(); j++)
		{
			if (j == i) continue;
			double rAngle = fabs(atan(rightLines[j][0]));
			double diffAngle = 180.0*fabs(thisAngle - rAngle)/CV_PI;

			//two lines is nearly parallel
			if ((diffAngle < 15.0) || (diffAngle > 60.0 && (rightLines[i][0] * rightLines[j][0]) < 0))
			{
				if (fabs(rightLines[i][1]) < fabs(rightLines[j][1]))
				{
					rightLines.erase(rightLines.begin() + j);
					rightPoints.erase(rightPoints.begin() + j);
					j -= 1;
				}

				else
				{
					rightLines.erase(rightLines.begin() + i);
					rightPoints.erase(rightPoints.begin() + i);
					i -= 1;
					break;
				}
			}
		}
	}

	//Check lanes' presence
	int lSize = leftLines.size();
	int rSize = rightLines.size();

	//Two lanes
	if (lSize > 0 && rSize > 0)
	{
		isLane = true;
		if (lSize == 1 && rSize == 1)
		{
			double diffAngle = 180.0 * (fabs(atan(leftLines[0][0]) - atan(rightLines[0][0])))/M_PI;
			//Straight lane
			if ((leftLines[0][0] * rightLines[0][0] < 0) && diffAngle > 20.0)
			{
				steeringAngle = 0.0;
			}
			//Curved
			else
			{
				if (diffAngle < 20.0)
				{
					double tmpAngle = 180.0 * fabs(atan(leftLines[0][0]))/M_PI;
					diffAngle = (tmpAngle > 90.0)? (180.0 - tmpAngle): tmpAngle;

					//Modify based on offset
					if (diffAngle > STRAIGHT_ANGLE_OFFSET)
					{
						direction = (leftLines[0][0] > 0)? 1: 2;;
						steeringAngle = (ANGLE_OFFSET - diffAngle) * COFF;
					}

					else
					{
						//Left curve
						if (leftLines[0][0] > 0)
						{
							direction = 1;	//mark as left curve lane
						}

						//Right curve
						else
						{
							direction = 2;	//mark as right curve lane
						}

						steeringAngle = ANGLE_OFFSET - diffAngle;
					}

				}

				else
				{
					//Left curve
					if (leftLines[0][0] > 0)
					{
						direction = 1;	//mark as left curve lane
					}

					//Right curve
					else
					{
						direction = 2;	//mark as right curve lane
					}

					steeringAngle = diffAngle;
				}
			}
		}

		else
		{
			//Detect left or right curve
			if (lSize == 1)
			{
				//Left Curve
				if ((rightLines[0][0] * rightLines[1][0] > 0) && rightLines[0][0] > 0)
				{
					direction = 1;	//mark as left curve lane
				}

				//Right curve
				else
				{
					direction = 2;	//mark as right curve lane
				}
			}

			else if (rSize == 1)
			{
				//Right Curve
				if ((leftLines[0][0] * leftLines[1][0] > 0) && leftLines[0][0] < 0)
				{
					direction = 2;	//mark as right curve lane
				}

				//Left curve
				else
				{
					direction = 1;
				}
			}

			else
			{
				//Curved
				if (leftLines[1][0] * rightLines[1][0] > 0)
				{
					//Left
					if (leftLines[1][0] > 0)
					{
						direction = 1;
					}

					//Right
					else
					{
						direction = 2;
					}
				}

				//Straight
				else
				{
					direction = 0;
				}
			}

			double lAngle = (lSize == 1)?0.0: (180.0*fabs(atan(leftLines[0][0]) - atan(leftLines[1][0]))/M_PI);
			lAngle = (lSize == 1)? 0.0: (leftLines[0][0] * leftLines[1][0] > 0)? lAngle: (180.0 - lAngle);

			double rAngle = (rSize == 1)?0.0: (180.0*fabs(atan(rightLines[0][0]) - atan(rightLines[1][0]))/M_PI);
			rAngle = (rSize == 1)? 0.0: (rightLines[0][0] * rightLines[1][0] > 0)? rAngle: (180.0 - rAngle);

			steeringAngle = (lAngle != 0 && rAngle != 0)? (lAngle + rAngle)/2.0: (lAngle != 0)? lAngle: rAngle;
		}
	}

	//Only left lane
	else if (lSize > 0)
	{
		isLane = true;
		if (lSize == 1)
		{
			double angle = 180.0 * fabs(atan(leftLines[0][0]))/M_PI;

			//Modify based on offset
			if (angle > STRAIGHT_ANGLE_OFFSET)
			{
				direction = (leftLines[0][0] > 0)? 1: 2;
				steeringAngle = (ANGLE_OFFSET - angle)*COFF;
			}

			else
			{
				//Left curve
				if (leftLines[0][0] > 0)
				{
					direction = 1;	//mark as left curve lane
				}

				//Right curve
				else
				{
					direction = 2;	//mark as right curve lane
				}

				steeringAngle = ANGLE_OFFSET - angle;
			}
		}
		else
		{
			double angle = (180.0*fabs(atan(leftLines[0][0]) - atan(leftLines[1][0]))/M_PI);

			//Right curve
			if (((leftLines[0][0] * leftLines[1][0]) > 0) && leftLines[0][0] < 0)
			{
				direction = 2;	//mark as right curve lane
			}

			//Left curve
			else
			{
				direction = 1;	//mark as left curve lane
			}

			steeringAngle = angle;
		}
	}

	//Only right lane
	else if (rSize > 0)
	{
		isLane = true;
		if (rSize == 1)
		{
			double angle = 180.0 * fabs(atan(rightLines[0][0]))/M_PI;

			//Modify based on offset
			if (angle > STRAIGHT_ANGLE_OFFSET)
			{
				direction = (rightLines[0][0] > 0)? 1: 2;
				steeringAngle = (ANGLE_OFFSET - angle)*COFF;
			}

			else
			{
				//Left curve
				if (rightLines[0][0] > 0)
				{
					direction = 1;	//mark as left curve lane
				}

				//Right curve
				else
				{
					direction = 2;	//mark as right curve lane
				}

				steeringAngle= ANGLE_OFFSET - angle;
			}
		}
		else
		{
			double angle = (180.0*fabs(atan(rightLines[0][0]) - atan(rightLines[1][0]))/M_PI);

			//Left curve
			if (((rightLines[0][0] * rightLines[1][0]) > 0) && rightLines[0][0] > 0)
			{
				direction = 1;	//mark as left curve lane
			}

			//Right curve
			else
			{
				direction = 2;	//mark as right curve lane
			}

			steeringAngle = angle;
		}
	}

	//No lane
	else
	{
		isLane = false;
	}

	if (steeringAngle > 90.0)
	{
		steeringAngle = 180.0 - steeringAngle;
	}

	root["isLane"] = isLane;
	root["angle"] = (direction == 1)? (steeringAngle): (direction == 2)? (-steeringAngle): 0.0;

	return root;
}

void LaneDetection::detectLane(bool laneColor)
{
	int numOfFrames = 0;
	while(1)
	{
		Json::Value root;

		boost::posix_time::ptime t1(boost::posix_time::microsec_clock::local_time());
		Mat oriImg, readImg = camController->getNewFrame();
		boost::posix_time::ptime t2(boost::posix_time::microsec_clock::local_time());

		resize(readImg, oriImg, Size(640, 480), 0, 0, INTER_LINEAR);
		Mat thImg = preProcessImage(oriImg, laneColor);
		root = detectLines(thImg);
		numOfFrames++;
		boost::posix_time::ptime t3(boost::posix_time::microsec_clock::local_time());

		boost::posix_time::time_duration duration = t2 - t1;
		duration = t3 - t2;

		//Make circular buffer thread safe
		laneResultMtx.lock();
		laneResult = root;
		laneResultMtx.unlock();
	}
}

Json::Value LaneDetection::getLaneResult()
{
	Json::Value root;
	laneResultMtx.lock();
	root = laneResult;
	laneResultMtx.unlock();

	return root;
}

void LaneDetection::startDetect()
{
	//Detect white lane
	boost::thread(&LaneDetection::detectLane, this, 1);
}
