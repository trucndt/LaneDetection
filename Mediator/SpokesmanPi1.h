#ifndef SPOKESMAN_H
#define SPOKESMAN_H

#include "Mediator.h"
#include <app/inc/Port.h>
#include <app/inc/Uart.h>

class Mediator;

class SpokesmanPi1
{
public:
    /**
     * @brief Constructor
     * @param The parent Mediator
     */
    SpokesmanPi1(Mediator* aMediator);

    /**
     * @brief initialize uart port
     * @return -1 if fail
     */
    int initialize();

    /**
     * @brief send messages to Pi2 and receive response
     * @param aMsg: Message to be sent
     * @param aResponse: Buffer to receive response
     * @return <0 if error, otherwise return the number of bytes received
     */
    int send(const char* aMsg, char* aResponse);

private:
    static const Bus UART_PORT = COM_BUS_UART_2;
    static const int MAX_SIZE_READ = 1024;

    /**
     * @brief Pointing to the parent Mediator instance
     */
    Mediator* mMediator;

    boost::mutex mMtx_Port;
};

#endif // SPOKESMAN_H
