//
// Created by trucndt on 5/12/17.
//

#include "LanelineControl.h"

using namespace std;

LanelineControl::LanelineControl(Mediator *aMediator,
                                 ServoController *aServoController,
                                 SpokesmanPi1 *aSpokesman)
{
    mMediator = aMediator;
    mServoController = aServoController;
    mSpokesman = aSpokesman;
    numOfFrames = 0;
}

void LanelineControl::start()
{
    boost::thread thread(boost::bind(&LanelineControl::run, this));
}

void LanelineControl::run()
{
    Json::Value root;
    root["action"] = "laneline";

    while (true)
    {
        usleep(QUERY_TIME_IN_MS * 1000);

        Json::FastWriter writer;
        char buf[1024];

        int size = mSpokesman->send(writer.write(root).c_str(), buf);
        buf[size] = NULL;

        cout << "RECEIVED: " << buf << endl;

        handlePositionUpdate(buf, size);
    }
}
void LanelineControl::handlePositionUpdate(const char *aMsg, int aMsgSize)
{
    Json::Reader reader;
    Json::Value root;

    if (reader.parse(aMsg, aMsg + aMsgSize, root, false) == false)
    {
        cout << "ERR: Cannot parse JSON" << endl;
        return;
    }

    numOfFrames++;
    bool isLane = root["isLane"].asBool();
    double angle = root["angle"].asDouble();
    cout << "Received: angle: " << angle << "  isLane: " << isLane << endl;

    //No lane detected
    if (isLane == false)
    {
    	mServoController->stopRobot();
    	cout << "Stop!\n";
    }

    else
    {
    	int leftRpm, rightRpm;
    	getWheelRpm(angle, leftRpm, rightRpm);
    	cout << "Left Rpm: " << leftRpm << endl;
    	cout << "Right Rpm: " << rightRpm << endl;

    	//Delay 0.5s
//    	usleep(100000);

    	mServoController->setRpm(rightRpm, leftRpm);
    }
    cout << "No. frame received: " << numOfFrames << endl;
}

void LanelineControl::getWheelRpm(float angle, int &leftRpm, int &rightRpm)
{
	angle += 90.0;
	angle = angle*M_PI/180.0;
	int moveRpm = (SPEED_M_S / SPEED_MAX) * 6000;
	int forwardRpm = sin(angle) * moveRpm;
	int turnRpm = fabs(cos(angle)) * moveRpm;

	if (turnRpm > forwardRpm)
	{
		turnRpm = forwardRpm;
	}

	//Left curved lane - leftRpm < rightRpm
	if (angle > M_PI/2)
	{
		leftRpm = -(forwardRpm - turnRpm);
		rightRpm = forwardRpm;
	}

	//Right curved lane - leftRpm > rightRpm
	else
	{
		leftRpm = -forwardRpm;
		rightRpm = forwardRpm - turnRpm;
	}

	leftRpm -= 500;
	rightRpm += 500;
}
