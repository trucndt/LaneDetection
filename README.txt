Dependencies:
  - OpenCV
  - Boost

Directory structure:
  - app/ : IES library
  - Mediator/ : Source code for Mediator
  - VisionExpert/ : Source code for VisionExpert
  - Streaming/ : Scripts to start streaming the Pi camera

To compile RCS:
  Execute these commands:
    $ cd rcsEmbedded
    $ mkdir build
    $ cd build
    $ cmake ..
    $ make

To cross-compile for Raspberry Pi:
  + Modify path to your RPi cross-compile toolchain in Toolchain-RaspberryPi.cmake
    SET(RPI_TOOLCHAIN_ROOT $ENV{HOME}/rpi/tools)

  + Execute these commands:
    $ cd rcsEmbedded
    $ mkdir build
    $ cd build
    $ cmake -D CMAKE_TOOLCHAIN_FILE=../Toolchain-RaspberryPi.cmake ..
    $ make

Live streaming: refer to the README.txt in Streaming/

CMake structure
rcsEmbedded/
├── app
├── CMakeLists.txt
├── Mediator
│   ├── CMakeLists.txt
├── VisionExpert
│   ├── CMakeLists.txt
└── README.txt